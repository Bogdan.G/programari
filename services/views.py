from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView
from django.http import HttpResponseRedirect

from homepage.models import Service
from homepage.forms import ServiceForm


def services_view(request):

    services = Service.objects.all()
    user = request.user

    # if this is a POST request we need to provess the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = ServiceForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            service = Service(name=form.cleaned_data['name'],
                              price=form.cleaned_data['price'],
                              duration=form.cleaned_data['duration'],
                              user=user)
            service.save()

            return HttpResponseRedirect('.')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ServiceForm()

    return render(request, 'services/services.html',
                  {'user': user,
                   'form': form,
                   'services': services})


class ServiceUpdate(UpdateView):
    model = Service
    template_name = 'services/service_form.html'
    form_class = ServiceForm


class ServiceDelete(DeleteView):
    model = Service
    success_url = reverse_lazy('services:services')
