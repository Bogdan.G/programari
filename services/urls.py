from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

urlpatterns = [
    path('<int:pk>/', views.ServiceUpdate.as_view(),
         name='service-update'),
    path(_('<int:pk>/delete/'), views.ServiceDelete.as_view(),
         name='service-delete'),
    path('', views.services_view, name='services'),
]
