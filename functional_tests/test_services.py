from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from django.contrib.auth.models import User
import time
import random
import string

from .test_homepage import login_user


class ServiceTests(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(ServiceTests, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        # Create user to have access to the site
        User.objects.create_user(username='bg', password='123')
        cls.user = User.objects.get(username='bg')

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(ServiceTests, cls).tearDownClass()

    def test_service_form(self):

        # User logs in
        login_user(self)
        num_of_tests = 5

        # Workaround for selenium
        time.sleep(1)
        
        # Go to the service form
        self.selenium.get(self.live_server_url + "/servicii/")

        # The user wants to add num_of_tests services
        for i in range(num_of_tests):

            # Get elements of the form
            name_input = self.selenium.find_element_by_id('id_name')
            price_input = self.selenium.find_element_by_id('id_price')
            duration_input = self.selenium.find_element_by_id('id_duration')
            form = self.selenium.find_element_by_id('add-service-form')

            # Create random name for the service
            min_chars = 8
            max_chars = 12
            allchar = string.ascii_letters
            random_name = "".join(
                random.choice(allchar)
                for x in range(random.randint(min_chars, max_chars))
            )

            # User fills in the form
            name_input.send_keys(random_name)
            price_input.send_keys("25")
            duration_input.clear()
            duration_input.send_keys("45:00")
            form.submit()

            # Needed for selenium to find the elements
            time.sleep(1)

        # Get the last service in the list to see if the form
        # accepted all of the users submits.
        service_item = self.selenium.find_element_by_xpath(
            f'/html/body/div/ul[{num_of_tests}]/li/a[1]')
        service_item.click()

        # Did the click redirected to the update page?
        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + "/servicii/1/")
