from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User
import datetime
import time
import random
import string

from homepage.models import Service, Provider
from homepage.forms import WorkingPlanForm
from .test_homepage import login_user


class ServiceTests(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(ServiceTests, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        # Create user to have access to the site
        User.objects.create_user(username='bg', password='123')
        cls.user = User.objects.get(username='bg')

        # Create some services for the providers
        Service.objects.create(name='interne', user=cls.user)
        Service.objects.create(name='consultatie', user=cls.user)
        Service.objects.create(name='manevre', user=cls.user)
        Service.objects.create(name='miscari', user=cls.user)
        Service.objects.create(name='cursuri', user=cls.user)
        Service.objects.create(name='chestii', user=cls.user)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(ServiceTests, cls).tearDownClass()

    def test_provider_form(self):

        # User logs in
        login_user(self)

        # This number is to facilitate future tests
        num_of_tests = len(Service.objects.all())
        num_of_service = 0

        # Workaround for selenium
        time.sleep(1)

        # Go to the provider form
        self.selenium.get(self.live_server_url + "/furnizori/")

        # The user wants to add num_of_tests providers
        for i in range(num_of_tests):

            # Get elements of the form
            last_name_input = self.selenium.find_element_by_id('id_last_name')
            first_name_input = self.selenium.find_element_by_id(
                'id_first_name')
            service_input = self.selenium.find_element_by_id(
                f'id_services_{num_of_service}')
            form = self.selenium.find_element_by_id('add-provider-form')

            # Create random first and last names for the providers
            min_chars = 8
            max_chars = 12
            allchar = string.ascii_letters
            random_last_name = "".join(
                random.choice(allchar)
                for x in range(random.randint(min_chars, max_chars))
            )
            random_first_name = "".join(
                random.choice(allchar)
                for x in range(random.randint(min_chars, max_chars))
            )

            # User fills in the form
            last_name_input.send_keys(random_last_name)
            first_name_input.send_keys(random_first_name)
            service_input.click()
            form.submit()

            # For every provider select a different service
            if(num_of_tests > num_of_service):
                num_of_service += 1

            # Needed for selenium to find the elements
            time.sleep(1)

        # Have all the services passed
        self.assertEqual(num_of_tests, num_of_service)

        # Get the last provider in the list to see if the form
        # accepted all of the users submits.
        provider_item = self.selenium.find_element_by_xpath(
            f'/html/body/div/ul[{num_of_tests}]/li/a[1]')
        provider_item.click()

        # Did the click redirected to the update page?
        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + "/furnizori/1/")

        # UDPATE PROVIDER

        # Test with incorrect values

        # Get elements for working plan
        monday_start_input = self.selenium.find_element_by_id(
            'id_monday_start')
        monday_end_input = self.selenium.find_element_by_id('id_monday_end')
        form = self.selenium.find_element_by_id('update-provider-form')
        monday_start_input.clear()
        monday_end_input.clear()
        monday_start_input.send_keys("13:00")
        monday_end_input.send_keys("12:00")
        form.submit()

        time.sleep(1)

        provider = Provider.objects.get(pk=1)
        error_item = self.selenium.find_element_by_xpath(
            '//*[@id="update-provider-form"]/ul/li').text
        self.assertEqual(
            error_item,
            "Timpul de inceput al zilei de lucru trebuie sa fie anterior timpului de final.")
        time.sleep(1)

        # Test with correct values

        # Get elements for working plan
        monday_start_input = self.selenium.find_element_by_id(
            'id_monday_start')
        monday_end_input = self.selenium.find_element_by_id('id_monday_end')
        form = self.selenium.find_element_by_id('update-provider-form')

        monday_start_input.clear()
        monday_end_input.clear()
        monday_start_input.send_keys("12:00")
        monday_end_input.send_keys("13:00")
        form.submit()

        time.sleep(1)

        provider = Provider.objects.get(pk=1)
        self.assertEqual(provider.working_plan.monday_start,
                         datetime.time(12, 0))
        time.sleep(1)

        # BREAKS

        # User wants to add breaks for provider

        # Test with incorrect values
        break_button = self.selenium.find_element_by_id('add-break')
        break_button.click()

        time.sleep(1)

        day_input = self.selenium.find_element_by_xpath(
            '//*[@id="id_day"]/option[3]')
        time_begin_input = self.selenium.find_element_by_id('id_time_begin')
        time_end_input = self.selenium.find_element_by_id('id_time_end')
        form = self.selenium.find_element_by_id('add-break-form')

        day_input.click()
        time_begin_input.send_keys("12:00")
        time_end_input.send_keys("11:00")
        form.submit()

        time.sleep(1)

        # Get value of error item from the list and test to see
        # that it is as expected
        error_item = self.selenium.find_element_by_xpath(
            '//*[@id="update-provider-form"]/ul/li').text
        self.assertEqual(
            error_item,
            "Timpul de final al pauze trebuie sa fie mai mare decat timpul de inceput.")

        time.sleep(1)

        # Test with correct values
        break_button = self.selenium.find_element_by_id('add-break')
        break_button.click()

        time.sleep(1)

        day_input = self.selenium.find_element_by_xpath(
            '//*[@id="id_day"]/option[3]')
        time_begin_input = self.selenium.find_element_by_id('id_time_begin')
        time_end_input = self.selenium.find_element_by_id('id_time_end')
        form = self.selenium.find_element_by_id('add-break-form')

        day_input.click()
        time_begin_input.clear()
        time_end_input.clear()
        time_begin_input.send_keys("12:00")
        time_end_input.send_keys("13:00")
        form.submit()

        time.sleep(1)

        # User wants to see the break he added in a list group
        # for that provider
        break_item = self.selenium.find_element_by_xpath(
            '//*[@id="break-end"]/ul/li').text

        self.assertEqual(break_item, "tuesday: 12:00 - 13:00")
