from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User
import time
import random
import string

from homepage.models import Service, Provider, WorkingPlan, Client

NUMBER_OF_SERVICES_AND_PROVIDERS = 13


def login_user(self):
    # User arrives at homepage
    self.selenium.get(self.live_server_url)

    # Login user
    input_username = self.selenium.find_element_by_id('id_username')
    input_username.send_keys('bg')
    input_password = self.selenium.find_element_by_id('id_password')
    input_password.send_keys('123')
    input_password.send_keys(Keys.ENTER)


class HomepageTests(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(HomepageTests, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        # Create user to have access to the site
        User.objects.create_user(username='bg', password='123')
        cls.user = User.objects.get(username='bg')

        # Populate services and providers
        # example service 1 - provider_ln 1 provider_fn 1
        for client_num in range(1, NUMBER_OF_SERVICES_AND_PROVIDERS):
            service = Service.objects.create(name='service %s' % client_num,
                                             user=cls.user)
            working_plan = WorkingPlan()
            working_plan.save()
            provider = Provider(
                last_name='provider_ln %s' % client_num,
                first_name='provider_fn %s' % client_num,
                user=cls.user, working_plan=working_plan
            )
            provider.save()
            provider.services.add(service)
            provider.save()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(HomepageTests, cls).tearDownClass()

    def test_user_can_add_appointments(self):

        # User logs in
        login_user(self)
        num_of_tests = 5
        time_number = 3

        for i in range(num_of_tests):
            # Random values for screen size,
            # first and last name of client
            width = random.randint(700, 1500)
            height = random.randint(700, 1200)
            min_chars = 8
            max_chars = 12
            allchar = string.ascii_letters
            random_first_name = "".join(
                random.choice(allchar)
                for x in range(random.randint(min_chars, max_chars))
            )
            random_last_name = "".join(
                random.choice(allchar)
                for x in range(random.randint(min_chars, max_chars))
            )

            # User wants to add a client / appointment
            # Click 'Adauga Programare' button
            self.selenium.find_element_by_id('addProvider').click()
            # Workaround for selenium to see the form
            self.selenium.set_window_size(width, height)

            # Get the elements of the form
            form = self.selenium.find_element_by_id('add-appointment-form')
            last_name = self.selenium.find_element_by_id('id_last_name')
            first_name = self.selenium.find_element_by_id('id_first_name')
            service = self.selenium.find_element_by_xpath(
                '//*[@id="id_service"]/option[2]')

            # Fill the form
            last_name.send_keys(random_first_name)
            first_name.send_keys(random_last_name)
            service.click()
            provider = self.selenium.find_element_by_xpath(
                '//*[@id="id_provider"]/option[2]')
            button_to_datetime = self.selenium.find_element_by_xpath(
                '//*[@id="to-datetime"]')
            provider.click()
            button_to_datetime.click()

            # Date-time pick: not dinamic; if possible, change in the future
            # possible failures can come from here
            day_select = self.selenium.find_element_by_xpath(
                '//*[@id="datetimepicker"]/div/ul/li[1]/div/div[1]/table/tbody/tr[4]/td[3]')
            day_select.click()
            time_select = self.selenium.find_element_by_xpath(
                '//*[@id="button-placeholder"]/div[' + str(time_number) + ']/button')
            time_select.click()
            time_number += 1

            # Submit
            form.submit()

            # Sleep to leave time for the elements to be visible for selenium
            time.sleep(2)
            
        # We want to make sure that the number of times we submitted the form(num_of_tests)
        # is equal to the number of the clients
        self.assertEqual(len(Client.objects.all()), num_of_tests)
