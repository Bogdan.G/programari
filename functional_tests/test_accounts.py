from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User
import time


class LoginTests(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(LoginTests, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        # Create user to have access to the site
        User.objects.create_user(username='bg', password='123')
        cls.user = User.objects.get(username='bg')

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(LoginTests, cls).tearDownClass()

    def test_login_form(self):

        # User arrives at homepage
        self.selenium.get(self.live_server_url)

        # Login user

        # Invalid values
        input_username = self.selenium.find_element_by_id('id_username')
        input_username.clear()
        input_username.send_keys('bgz')
        input_password = self.selenium.find_element_by_id('id_password')
        input_password.clear()
        input_password.send_keys('123')
        input_password.send_keys(Keys.ENTER)

        time.sleep(1)
        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + '/accounts/login/?next=/')
        time.sleep(1)

        # Valid values
        input_username = self.selenium.find_element_by_id('id_username')
        input_username.clear()
        input_username.send_keys('bg')
        input_password = self.selenium.find_element_by_id('id_password')
        input_password.clear()
        input_password.send_keys('123')
        input_password.send_keys(Keys.ENTER)

        time.sleep(1)
        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + '/')

        # Logout
        user_button = self.selenium.find_element_by_id("dropdown-user-button")
        user_button.click()
        logout_href = self.selenium.find_element_by_id("logout")
        logout_href.click()

        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + '/accounts/login/')

        time.sleep(1)

    def test_signup(self):

        # User arrives at homepage
        self.selenium.get(self.live_server_url)

        signup_button = self.selenium.find_element_by_id("sign-up-button")
        signup_button.click()

        # Test invalid

        # Different passwords
        username = self.selenium.find_element_by_id("id_username")
        email = self.selenium.find_element_by_id("id_email")
        password1 = self.selenium.find_element_by_id("id_password1")
        password2 = self.selenium.find_element_by_id("id_password2")
        form = self.selenium.find_element_by_id('sign-up-form')

        username.send_keys("voltan")
        email.send_keys("as@not.mas")
        password1.send_keys("enoxiumopm")
        password2.send_keys("enoxiumopmzzz")
        form.submit()

        time.sleep(1)
        error_item = self.selenium.find_element_by_xpath(
            '//*[@id="error-password2"]/div/span/ul/li').text
        self.assertEqual(error_item, "Cele doua parole sunt diferite.")

        time.sleep(1)

        # Passwords incorrect format
        username = self.selenium.find_element_by_id("id_username")
        email = self.selenium.find_element_by_id("id_email")
        password1 = self.selenium.find_element_by_id("id_password1")
        password2 = self.selenium.find_element_by_id("id_password2")
        form = self.selenium.find_element_by_id('sign-up-form')

        username.clear()
        username.send_keys("voltan")
        email.clear()
        email.send_keys("as@not.mas")
        password1.clear()
        password1.send_keys("enox")
        password2.clear()
        password2.send_keys("enox")
        form.submit()

        time.sleep(1)
        error_item = self.selenium.find_element_by_xpath(
            '//*[@id="error-password2"]/div/span/ul/li').text
        self.assertEqual(
            error_item, "Parola este prea scurtă. Trebuie să conțină cel puțin 8 caractere.")

        time.sleep(1)

        # Invalid email
        username = self.selenium.find_element_by_id("id_username")
        email = self.selenium.find_element_by_id("id_email")
        password1 = self.selenium.find_element_by_id("id_password1")
        password2 = self.selenium.find_element_by_id("id_password2")
        form = self.selenium.find_element_by_id('sign-up-form')

        username.clear()
        username.send_keys("voltan")
        email.clear()
        email.send_keys("as@not")
        password1.clear()
        password1.send_keys("enoxiumopm")
        password2.clear()
        password2.send_keys("enoxiumopm")
        form.submit()

        time.sleep(1)
        error_item = self.selenium.find_element_by_xpath(
            '//*[@id="error-email"]/div/span/ul/li').text
        self.assertEqual(error_item, "Introduceți o adresă de email validă.")

        time.sleep(1)

        # Test valid

        username = self.selenium.find_element_by_id("id_username")
        email = self.selenium.find_element_by_id("id_email")
        password1 = self.selenium.find_element_by_id("id_password1")
        password2 = self.selenium.find_element_by_id("id_password2")
        form = self.selenium.find_element_by_id('sign-up-form')

        username.clear()
        username.send_keys("voltan")
        email.clear()
        email.send_keys("as@not.is")
        password1.clear()
        password1.send_keys("enoxiumopm")
        password2.clear()
        password2.send_keys("enoxiumopm")
        form.submit()

        time.sleep(1)

        self.assertEqual(self.selenium.current_url,
                         self.live_server_url + "/")
        self.assertEqual(User.objects.get(username="voltan").username,
                         "voltan")

        time.sleep(1)
