from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

urlpatterns = [
    path('<int:pk>/', views.ClientUpdate.as_view(),
         name='client-update'),
    path(_('<int:pk>/delete/'), views.ClientDelete.as_view(),
         name='client-delete'),
    path('', views.clients_view, name='clients'),
]
