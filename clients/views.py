from django.shortcuts import render
from django.views.generic import UpdateView, DeleteView
from django.urls import reverse_lazy

from homepage.models import Client
from homepage.forms import ClientForm


def clients_view(request):

    user = request.user
    print(user.client_set.all())

    clients = user.client_set.all()

    return render(request, 'clients/clients.html',
                  {'clients': clients})


class ClientUpdate(UpdateView):
    model = Client
    template_name = 'clients/client_form.html'
    form_class = ClientForm
    user = None

    def get_form_kwargs(self):
        kwargs = super(ClientUpdate, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class ClientDelete(DeleteView):
    model = Client
    success_url = reverse_lazy('clients:clients')
