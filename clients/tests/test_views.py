from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils import timezone

from homepage.models import Client, Service, Provider, WorkingPlan


# Create 13 clients for pagination tests
NUMBER_OF_CLIENTS = 13


class ClientViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):

        for client_num in range(NUMBER_OF_CLIENTS):
            user = User.objects.create_user(
                username='User %s' % client_num,
                password='passwd%s' % client_num)
            service = Service(name='service %s' % client_num,
                              user=user)
            service.save()
            working_plan = WorkingPlan()
            working_plan.save()
            provider = Provider(
                last_name='provln %s' % client_num,
                first_name='provfn %s' % client_num,
                user=user, working_plan=working_plan
            )
            provider.save()
            provider.services.add(service)
            provider.save()
            client = Client(
                last_name='clientln %s' % client_num,
                first_name='clientfn %s' % client_num,
                date_time=timezone.now(),
                service=service,
                provider=provider
            )
            client.save()

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get('/clienti/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('clients:clients'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('clients:clients'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'clients/clients.html')

    def test_lists_all_clients(self):
        resp = self.client.get(reverse('clients:clients'))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual((len(resp.context['clients'])), NUMBER_OF_CLIENTS)

# TODO: Test that the clients belong to a specific user
