from django.contrib import admin

from .models import Service, Provider, Client


admin.site.register(Service)
admin.site.register(Provider)
admin.site.register(Client)
