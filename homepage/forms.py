from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordChangeForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.utils.translation import gettext as _
import datetime

from .models import Client, Service, Provider, Break, WorkingPlan


class CustomCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    template_name = 'homepage/multiple_input.html'


class UserLoginForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(UserLoginForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ['username', 'password', ]
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'}),
        }


class CustomUserCreationForm(UserCreationForm):

    error_messages = {
        'password_mismatch': _('The passwords are different.'),
    }
    password1 = forms.CharField(
        label=_('Password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_('Confirm password'),
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        strip=False,
        help_text=_('Introduce the same password, for verification.'),
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('email', )
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }


class UserAccountForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(UserAccountForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ['username', 'password', 'email']
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }


class CustomPasswordChangeForm(PasswordChangeForm):

    old_password = forms.CharField(
        label=_('Old password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'autofocus': True,
                                          'class': 'form-control'}),
    )
    new_password1 = forms.CharField(
        label=_('New password'),
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_('Confirm new password'),
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )

    class Meta:
        fields = ['old_password', 'new_password1', 'new_password2', ]
        widgets = {
            'old_password': forms.PasswordInput(attrs={'class': 'form-control'}),
            'new_password1': forms.PasswordInput(attrs={'class': 'form-control'}),
            'new_password2': forms.PasswordInput(attrs={'class': 'form-control'}),
        }


class ServiceForm(forms.ModelForm):

    class Meta:
        model = Service
        fields = ['name', 'price', 'duration', ]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'price': forms.TextInput(
                attrs={'class': 'form-control', }),
            'duration': forms.TimeInput(
                attrs={'class': 'form-control'},
                format='%H:%M')
        }


class ProviderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ProviderForm, self).__init__(*args, **kwargs)
        self.fields['services'] = forms.ModelMultipleChoiceField(
            label=_('Services'),
            queryset=self.user.service_set.all(),
            widget=CustomCheckboxSelectMultiple())

    class Meta:
        model = Provider
        fields = ['first_name', 'last_name', 'services']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class WorkingPlanForm(forms.ModelForm):

    class Meta:
        model = WorkingPlan
        fields = '__all__'
        widgets = {
            'monday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'monday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'tuesday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'tuesday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'wednesday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'wednesday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'thursday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'thursday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'friday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'friday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'saturday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'saturday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'sunday_start': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
            'sunday_end': forms.DateTimeInput(
                attrs={'class': 'form-control timepicker', 'readonly': True},
                format='%H:%M'),
        }

    def clean(self):
        cleaned_data = super().clean()
        monday_start = cleaned_data.get("monday_start")
        monday_end = cleaned_data.get("monday_end")
        tuesday_start = cleaned_data.get("tuesday_start")
        tuesday_end = cleaned_data.get("tuesday_end")
        wednesday_start = cleaned_data.get("wednesday_start")
        wednesday_end = cleaned_data.get("wednesday_end")
        thursday_start = cleaned_data.get("thursday_start")
        thursday_end = cleaned_data.get("thursday_end")
        friday_start = cleaned_data.get("friday_start")
        friday_end = cleaned_data.get("friday_end")
        saturday_start = cleaned_data.get("saturday_start")
        saturday_end = cleaned_data.get("saturday_end")
        sunday_start = cleaned_data.get("sunday_start")
        sunday_end = cleaned_data.get("sunday_end")

        def compare_start_end(start, end):
            tb = datetime.datetime(
                1, 1, 1, start.hour, start.minute)
            te = datetime.datetime(
                1, 1, 1, end.hour, end.minute)
            print(tb >= te)
            if tb >= te:
                raise forms.ValidationError(
                    _('Beginning time of workday must be less than ending time.')
                )

        compare_start_end(monday_start, monday_end)
        compare_start_end(tuesday_start, tuesday_end)
        compare_start_end(wednesday_start, wednesday_end)
        compare_start_end(thursday_start, thursday_end)
        compare_start_end(friday_start, friday_end)
        compare_start_end(saturday_start, saturday_end)
        compare_start_end(sunday_start, sunday_end)
        print(self.cleaned_data)
        return self.cleaned_data


class BreakForm(forms.ModelForm):

    class Meta:
        model = Break
        fields = ['day', 'time_begin', 'time_end']
        widgets = {
            'day': forms.Select(attrs={'class': 'form-control'}),
            'time_begin': forms.DateTimeInput(
                attrs={'class': 'form-control', 'readonly': True},
                format='%H:%M'),
            'time_end': forms.DateTimeInput(
                attrs={'class': 'form-control', 'readonly': True},
                format='%H:%M'),
        }

    def clean(self):
        cleaned_data = super().clean()
        time_begin = cleaned_data.get("time_begin")
        time_end = cleaned_data.get("time_end")

        if time_begin and time_end:
            tb = datetime.datetime(
                1, 1, 1, time_begin.hour, time_begin.minute)
            te = datetime.datetime(
                1, 1, 1, time_end.hour, time_end.minute)
            if tb >= te:
                raise forms.ValidationError(
                    _('Beginning time of the break must be less than ending time.')
                )
        return self.cleaned_data

        # TODO: break time must be between start and end time


class ClientForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ClientForm, self).__init__(*args, **kwargs)
        self.fields['service'] = forms.ModelChoiceField(
            label=_('Service'),
            queryset=self.user.service_set.all(),
            widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Client
        fields = ['first_name', 'last_name',
                  'date_time', 'service', 'provider']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'date_time': forms.DateTimeInput(
                attrs={'class': 'form-control', 'readonly': True},
                format='%Y-%m-%d %H:%M'),
            'provider': forms.Select(attrs={'class': 'form-control'}),
        }
