from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from datetime import timedelta
from django.utils.translation import gettext_lazy as _


class Service(models.Model):
    name = models.CharField(_('name'), max_length=200)
    price = models.DecimalField(_('price'),
                                max_digits=10, decimal_places=2,
                                blank=True, null=True)
    duration = models.TimeField(_('duration'), blank=True,
                                default='00:30')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name=_('user'))

    class Meta:
        verbose_name = _('service')
        verbose_name_plural = _('services')

    def __str__(self):
        return f'{self.name.capitalize()}'

    def get_absolute_url(self):
        return reverse('services:services')


class WorkingPlan(models.Model):
    time_start = '00:00'
    time_end = '23:59'

    monday_start = models.TimeField(default=time_start)
    monday_end = models.TimeField(default=time_end)
    tuesday_start = models.TimeField(default=time_start)
    tuesday_end = models.TimeField(default=time_end)
    wednesday_start = models.TimeField(default=time_start)
    wednesday_end = models.TimeField(default=time_end)
    thursday_start = models.TimeField(default=time_start)
    thursday_end = models.TimeField(default=time_end)
    friday_start = models.TimeField(default=time_start)
    friday_end = models.TimeField(default=time_end)
    saturday_start = models.TimeField(default=time_start)
    saturday_end = models.TimeField(default=time_end)
    sunday_start = models.TimeField(default=time_start)
    sunday_end = models.TimeField(default=time_end)


class Provider(models.Model):
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    services = models.ManyToManyField(Service, verbose_name=_('services'))
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name=_('user'))
    working_plan = models.OneToOneField(
        WorkingPlan, on_delete=models.PROTECT, verbose_name=_('working plan'))

    class Meta:
        verbose_name = _('provider')
        verbose_name_plural = _('providers')

    def __str__(self):
        return f'{self.first_name.capitalize() + " " + self.last_name.capitalize()}'

    def get_absolute_url(self):
        return reverse('providers:providers')


class Break(models.Model):

    DAYS_OF_WEEK = (
        ('monday', _('Monday')),
        ('tuesday', _('Tuesday')),
        ('wednesday', _('Wednesday')),
        ('thursday', _('Thursday')),
        ('friday', _('Friday')),
        ('saturday', _('Saturday')),
        ('sunday', _('Sunday'))
    )
    day = models.CharField(_('day'), max_length=20, choices=DAYS_OF_WEEK)
    time_begin = models.TimeField(_('time begin'))
    time_end = models.TimeField(_('time end'))
    provider = models.ForeignKey(
        Provider, on_delete=models.CASCADE, verbose_name=_('provider'))

    class Meta:
        verbose_name = _('break')
        verbose_name_plural = _('breaks')

    def __str__(self):
        return '{0}: {1} - {2}'.format(
            self.day.capitalize(),
            self.time_begin.strftime("%H:%M"),
            self.time_end.strftime("%H:%M"))


class Client(models.Model):
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    date_time = models.DateTimeField(_('date and time'), default=timezone.now)
    email = models.EmailField(blank=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name=_('user'))
    service = models.ForeignKey(Service,
                                on_delete=models.CASCADE,
                                verbose_name=_('service'))
    provider = models.ForeignKey(Provider,
                                 on_delete=models.CASCADE,
                                 verbose_name=_('provider'))

    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def __str__(self):
        if (type(self.date_time) != str):
            return f'{self.first_name.capitalize()}-{self.last_name.capitalize()} {self.date_time.strftime("%Y-%m-%d %H:%M")}'
        else:
            return f'{self.first_name.capitalize()}-{self.last_name.capitalize()} {self.date_time}'

    def get_absolute_url(self):
        return reverse('clients:clients')

    def get_date(self):
        """Return the client's appointment date."""
        return '%s' % (self.date_time)

    def get_full_name(self):
        """Return the client's full name."""
        return '%s %s' % (self.first_name.capitalize(),
                          self.last_name.capitalize())


# TODO: Do not leave blank=True or null=True in production
