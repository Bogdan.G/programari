"""The view logic for the first page."""
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.translation import (gettext_lazy as _,
                                      get_language)
import datetime

from .models import Service, Provider, Client
from .forms import ClientForm


# Helper functions
def strToMinutes(element):
    time = element.split(':')
    hours = int(time[0])
    minutes = int(time[1])
    return (hours * 60) + minutes


def parse_ymd_datetime(s):
    year_s, mon_s, day_s = s.split('-')
    return datetime.datetime(int(year_s), int(mon_s), int(day_s))


def parse_ymd_date(s):
    year_s, mon_s, day_s = s.split('-')
    return datetime.date(int(year_s), int(mon_s), int(day_s))


def parse_hm(s):
    hour_s, min_s = s.split(':')
    return datetime.time(int(hour_s), int(min_s))


def check_for_breaks(e, es, bb, be):
    """
    Check to see if we arrive on a break.

    e=event, es=event+service, bb=break_begin, be=break_end
    """
    if(((e < bb) and (es > bb)) or ((e < be) and (es > be)) or
       ((e >= bb) and (es <= be))):
        return True
    return False


def check_for_clients(e, es, cb, ce):
    """
    Check to see if we arrive on a client's time.

    e=event, es=event+service, cb=client_begin, ce=client_end
    """
    if(((e < cb) and (es > cb)) or ((e < ce) and (es > ce)) or
       ((e >= cb) and (es <= ce))):
        return True
    return False


@login_required
def homepage_view(request):
    """Appointment form and FullCalendar."""

    user = request.user

    # respond to ajax request for the ClientForm
    if request.is_ajax():
        # if this is a request for data on service change
        if (request.POST.get('request') == "service"):

            service_pk = request.POST.get('service_pk')
            if (service_pk != ''):

                service = user.service_set.get(pk=service_pk)
                service_providers = ''
                service_providers_pk = ''

                for provider in service.provider_set.all():
                    service_providers += str(provider) + ","
                    service_providers_pk += str(provider.pk) + ","

                data = {
                    'providers': service_providers,
                    'providers_pk': service_providers_pk
                }

            else:
                data = {
                    'providers': "",
                    'providers_pk': ""
                }

            return JsonResponse(data)

        # if this is a request for data on provider change
        if (request.POST.get('request') == "provider"):

            provider_pk = request.POST.get('provider_pk')
            service_pk = request.POST.get('service_pk')

            if (provider_pk != '' and service_pk != ''):

                provider = Provider.objects.get(pk=provider_pk)
                service = Service.objects.get(pk=service_pk)

                service_duration = strToMinutes(str(service.duration))

                business_hours_start = ''
                business_hours_end = ''

                business_hours_start += str(
                    provider.working_plan.monday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.monday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.tuesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.tuesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.wednesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.wednesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.thursday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.thursday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.friday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.friday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.saturday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.saturday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.sunday_start.strftime("%H:%M"))
                business_hours_end += str(
                    provider.working_plan.sunday_end.strftime("%H:%M"))

                break_day = ''
                break_begin = ''
                break_end = ''

                for br in provider.break_set.all():
                    break_day += str(br.day) + ','
                    break_begin += str(br.time_begin.strftime("%H:%M")) + ','
                    break_end += str(br.time_end.strftime("%H:%M")) + ','

                data = {
                    'service_duration': service_duration,
                    'business_hours_start': business_hours_start,
                    'business_hours_end': business_hours_end,
                    'break_day': break_day,
                    'break_begin': break_begin,
                    'break_end': break_end,
                }

                return JsonResponse(data)

        # if this is a request for data on calendar date_time change
        if (request.POST.get('request') == "times"):

            provider_pk = request.POST.get('provider_pk')
            service_pk = request.POST.get('service_pk')
            date_selected = request.POST.get('date_selected')

            if (provider_pk != '' and service_pk != '' and
                    date_selected != ''):

                provider = Provider.objects.get(pk=provider_pk)
                service = Service.objects.get(pk=service_pk)

                service_duration = strToMinutes(str(service.duration))

                dt = parse_ymd_date(date_selected)
                day = dt.strftime("%A")
                if (day == 'Monday' or day == 'Luni'):
                    day = 'monday'
                elif (day == 'Tuesday' or day == 'Marți'):
                    day = 'tuesday'
                elif (day == 'Wednesday' or day == 'Miercuri'):
                    day = 'wednesday'
                elif (day == 'Thursday' or day == 'Joi'):
                    day = 'thursday'
                elif (day == 'Friday' or day == 'Vineri'):
                    day = 'friday'
                elif (day == 'Saturday' or day == 'Sâmbătă'):
                    day = 'saturday'
                elif (day == 'Sunday' or day == 'Duminică'):
                    day = 'sunday'

                client_times_begin = []
                client_times_end = []
                br_begin = []
                br_end = []
                times = []
                begin = 0
                end = 0
                e = 0
                i = 0
                j = 0

                for client in provider.client_set.all():
                    if (client.date_time.strftime('%Y-%m-%d') ==
                            date_selected):
                        client_times_begin.append(
                            strToMinutes(str(
                                client.date_time.time().strftime("%H:%M"))))
                client_times_begin.sort()

                for tm in client_times_begin:
                    client_times_end.append(tm + service_duration)

                for br in provider.break_set.filter(day=day):
                    br_begin.append(strToMinutes(str(br.time_begin)))
                    br_end.append(strToMinutes(str(br.time_end)))
                br_begin.sort()
                br_end.sort()

                if (day == 'monday'):
                    begin = strToMinutes(
                        str(provider.working_plan.monday_start))
                    end = strToMinutes(str(provider.working_plan.monday_end))
                elif (day == 'tuesday'):
                    begin = strToMinutes(
                        str(provider.working_plan.tuesday_start))
                    end = strToMinutes(str(provider.working_plan.tuesday_end))
                elif (day == 'wednesday'):
                    begin = strToMinutes(
                        str(provider.working_plan.wednesday_start))
                    end = strToMinutes(
                        str(provider.working_plan.wednesday_end))
                elif (day == 'thursday'):
                    begin = strToMinutes(
                        str(provider.working_plan.thursday_start))
                    end = strToMinutes(str(provider.working_plan.thursday_end))
                elif (day == 'friday'):
                    begin = strToMinutes(
                        str(provider.working_plan.friday_start))
                    end = strToMinutes(str(provider.working_plan.friday_end))
                elif (day == 'saturday'):
                    begin = strToMinutes(
                        str(provider.working_plan.saturday_start))
                    end = strToMinutes(str(provider.working_plan.saturday_end))
                elif (day == 'sunday'):
                    begin = strToMinutes(
                        str(provider.working_plan.sunday_start))
                    end = strToMinutes(str(provider.working_plan.sunday_end))

                def format_as_hour(e):
                    hours = "{:02d}".format(int(e / 60))
                    minutes = "{:02d}".format(e % 60)
                    return hours + ":" + minutes

                e = begin

                while(e < end):
                    if((i < len(br_begin)) and
                            (check_for_breaks(e, e + service_duration,
                                              br_begin[i], br_end[i]))):
                        e = br_end[i]
                        i += 1
                        continue
                    elif((j < len(client_times_begin)) and
                         (check_for_clients(
                             e, e + service_duration,
                             client_times_begin[j], client_times_end[j]))):
                        e = client_times_end[j]
                        j += 1
                        continue

                    hours = "{:02d}".format(int(e / 60))
                    minutes = "{:02d}".format(e % 60)
                    times.append(hours + ":" + minutes)
                    e += service_duration

                if (e > end):
                    times.pop()
                data = {
                    "times": times,
                }

                return JsonResponse(data)

        # respond to ajax request from the Calendar dropdown
        # (get clients for the provider)
        if (request.POST.get('request') == "clients"):
            provider_pk = request.POST.get('provider_pk')

            if (provider_pk != ''):
                provider = Provider.objects.get(pk=provider_pk)
                clients = provider.client_set.all()
                request.session['provider'] = str(provider_pk)

                clients_pks = ''
                clients_dates = ''
                clients_names = ''
                clients_services = ''
                clients_services_duration = ''

                for client in clients:
                    clients_pks += str(client.pk) + ','
                    clients_names += str(client.get_full_name()) + ','
                    clients_dates += str(client.get_date()) + ','
                    clients_services += str(client.service) + ','
                    clients_services_duration += str(strToMinutes(str(
                        client.service.duration))) + ','

                business_hours_start = ''
                business_hours_end = ''

                business_hours_start += str(
                    provider.working_plan.monday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.monday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.tuesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.tuesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.wednesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.wednesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.thursday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.thursday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.friday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.friday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.saturday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    provider.working_plan.saturday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    provider.working_plan.sunday_start.strftime("%H:%M"))
                business_hours_end += str(
                    provider.working_plan.sunday_end.strftime("%H:%M"))

                break_day = ''
                break_begin = ''
                break_end = ''
                br_day = {'monday': 1, 'tuesday': 2, 'wednesday': 3, 'thursday': 4,
                          'friday': 5, 'saturday': 6, 'sunday': 0}
                day_of_break = ''

                for br in provider.break_set.all():
                    break_day += str(br.day) + ','
                    break_begin += str(br.time_begin.strftime("%H:%M")) + ','
                    break_end += str(br.time_end.strftime("%H:%M")) + ','

                break_day_arr = break_day.split(",")
                break_day_arr.pop()

                for day in break_day_arr:
                    day_of_break += str(br_day[day]) + ','

                data = {
                    'clientsPks': clients_pks,
                    'clientsNames': clients_names,
                    'clientsDates': clients_dates,
                    'clientServices': clients_services,
                    'clientServicesDuration': clients_services_duration,
                    'business_hours_start': business_hours_start,
                    'business_hours_end': business_hours_end,
                    'break_begin': break_begin,
                    'break_end': break_end,
                    'break_day': break_day,
                    'day_of_break': day_of_break,
                }

                return JsonResponse(data)

        # Respond to ajax request for day of break change
        if (request.POST.get('request') == 'day_for_breaks'):

            provider_pk = request.POST.get('provider_pk')
            day_selected = request.POST.get('day_selected')

            provider = Provider.objects.get(pk=provider_pk)

            begin = ''
            end = ''

            if (day_selected == 'monday'):
                begin = str(provider.working_plan.monday_start)
                end = str(provider.working_plan.monday_end)
            elif (day_selected == 'tuesday'):
                begin = str(provider.working_plan.tuesday_start)
                end = str(provider.working_plan.tuesday_end)
            elif (day_selected == 'wednesday'):
                begin = str(provider.working_plan.wednesday_start)
                end = str(provider.working_plan.wednesday_end)
            elif (day_selected == 'thursday'):
                begin = str(provider.working_plan.thursday_start)
                end = str(provider.working_plan.thursday_end)
            elif (day_selected == 'friday'):
                begin = str(provider.working_plan.friday_start)
                end = str(provider.working_plan.friday_end)
            elif (day_selected == 'saturday'):
                begin = str(provider.working_plan.saturday_start)
                end = str(provider.working_plan.saturday_end)
            elif (day_selected == 'sunday'):
                begin = str(provider.working_plan.sunday_start)
                end = str(provider.working_plan.sunday_end)

            data = {
                'begin': begin,
                'end': end,
            }

            return JsonResponse(data)

    # not ajax request
    else:
        user = request.user
        providers = user.provider_set.all()
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            # create a form instance and populate it with
            # data from the request:
            form = ClientForm(request.POST, user=user)
            # check whether it's valid:
            if form.is_valid():
                # process the data in form.cleaned_data as required
                # ...

                first_name = form.cleaned_data['first_name']
                last_name = form.cleaned_data['last_name']
                date_time = form.cleaned_data['date_time']
                service = form.cleaned_data['service']
                provider = form.cleaned_data['provider']

                client = Client(first_name=first_name, last_name=last_name,
                                date_time=date_time, service=service, provider=provider, user=user)

                client.save()

                return HttpResponseRedirect('.')

            else:

                # TODO: fill this out
                pass

        # if a GET (or any other method) we'll create a blank form
        else:

            form = ClientForm(user=user)
            form.user = user

            user = request.user
            providers = user.provider_set.all()

            if ('provider' in request.session):

                session_provider = Provider.objects.get(
                    pk=request.session['provider'])

                clients = session_provider.client_set.all()

                clients_pks = ''
                clients_dates = ''
                clients_names = ''
                clients_services = ''
                clients_services_duration = ''

                for client in clients:
                    clients_pks += str(client.pk) + ','
                    clients_names += str(client.get_full_name()) + ','
                    clients_dates += str(client.get_date()) + ','
                    clients_services += str(client.service) + ','
                    clients_services_duration += str(strToMinutes(str(
                        client.service.duration))) + ','

                business_hours_start = ''
                business_hours_end = ''

                business_hours_start += str(
                    session_provider.working_plan.monday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.monday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.tuesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.tuesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.wednesday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.wednesday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.thursday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.thursday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.friday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.friday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.saturday_start.strftime("%H:%M")) + ','
                business_hours_end += str(
                    session_provider.working_plan.saturday_end.strftime("%H:%M")) + ','
                business_hours_start += str(
                    session_provider.working_plan.sunday_start.strftime("%H:%M"))
                business_hours_end += str(
                    session_provider.working_plan.sunday_end.strftime("%H:%M"))

                break_day = ''
                break_begin = ''
                break_end = ''
                br_day = {'monday': 1, 'tuesday': 2, 'wednesday': 3, 'thursday': 4,
                          'friday': 5, 'saturday': 6, 'sunday': 0}
                day_of_break = ''

                for br in session_provider.break_set.all():
                    break_day += str(br.day) + ','
                    break_begin += str(br.time_begin.strftime("%H:%M")) + ','
                    break_end += str(br.time_end.strftime("%H:%M")) + ','

                break_day_arr = break_day.split(",")
                break_day_arr.pop()

                for day in break_day_arr:
                    day_of_break += str(br_day[day]) + ','

                return render(request, 'homepage/calendar.html',
                              {'form': form,
                               'providers': providers,
                               'session_provider': session_provider,
                               'clients_pks': clients_pks,
                               'clients_names': clients_names,
                               'clients_dates': clients_dates,
                               'clientServices': clients_services,
                               'clientServicesDuration': clients_services_duration,
                               'business_hours_start': business_hours_start,
                               'business_hours_end': business_hours_end,
                               'break_begin': break_begin,
                               'break_end': break_end,
                               'break_day': break_day,
                               'day_of_break': day_of_break, })

        # case all else fails
        # TODO: add better logic for the return statements

        lang = get_language()

        return render(request, 'homepage/calendar.html',
                      {'form': form,
                       'providers': providers,
                       'lang': lang, })


def search_results(request):
    """Search functionality."""
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']

        full_name = Q(last_name__icontains=q) | Q(first_name__icontains=q)
        clients = Client.objects.filter(full_name)
        providers = Provider.objects.filter(full_name)
        services = Service.objects.filter(name__icontains=q)

        return render(request, 'homepage/search-results.html',
                      {
                          'clients': clients,
                          'providers': providers,
                          'services': services,
                          'query': q
                      })
    else:
        return HttpResponse(_('Please enter a search term.'))
