from django.test import TestCase
from django.contrib.auth.models import User
import datetime

from homepage.models import (Service, Provider,
                             WorkingPlan, Break)


def parse_hm(s):
    hour_s, min_s = s.split(':')
    return datetime.time(int(hour_s), int(min_s))


class ModelsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='bg', password='passwd')
        cls.user = User.objects.get(username='bg')
        Service.objects.create(name='interne', user=cls.user)
        Service.objects.create(name='consultatie', user=cls.user)
        cls.service1 = Service.objects.get(name="interne")
        cls.service2 = Service.objects.get(name="consultatie")

    def test_user(self):
        username_label = self.user._meta.get_field('username').verbose_name
        password_label = self.user._meta.get_field('password').verbose_name
        self.assertEqual(username_label, 'nume utilizator')
        self.assertEqual(password_label, 'parolă')

    def test_service(self):

        # Test basic attributes for sanity check
        field_label = self.service1._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'nume')
        max_length = self.service1._meta.get_field('name').max_length
        self.assertEqual(max_length, 200)

        # Absolute url
        self.assertEqual(self.service1.get_absolute_url(), '/servicii/')

        # Test str method
        self.assertEqual(str(self.service1), 'Interne')

        # If we delete a user the service no longer exists
        self.user.delete()
        with self.assertRaisesMessage(
                Service.DoesNotExist,
                'Service matching query does not exist.'):
            Service.objects.get(name='interne')

    def test_provider(self):
        # Create variables which a provider will need
        working_plan = WorkingPlan()
        working_plan.save()

        last_name = "Ikn"
        first_name = "Hnm"

        provider = Provider(last_name=last_name, first_name=first_name,
                            user=self.user, working_plan=working_plan)
        provider.save()
        provider.services.add(self.service1, self.service2)
        provider.save()
        self.assertEqual(str(provider), last_name + " " + first_name)

        # Create a break and add to provider
        br = Break.objects.create(
            day='monday', time_begin=parse_hm("12:00"),
            time_end=parse_hm("13:00"), provider=provider)
        provider.break_set.add(br)
        provider.save()
        self.assertEqual(str(provider.break_set.get(day='monday')),
                         'Luni: 12:00 - 13:00')
