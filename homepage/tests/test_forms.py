from django.test import TestCase
from django.contrib.auth.models import User

from homepage.models import Service, Provider, WorkingPlan
from homepage.forms import ClientForm


class FormsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        """Create user, service, working_plan, provider and ClientForm."""
        cls.user = User.objects.create_user(username='bg', password='e')
        cls.form = ClientForm(user=cls.user)
        cls.service = Service(name='interne', user=cls.user)
        cls.service.save()
        cls.provider = Provider(
            last_name='ion', first_name='dumitru', user=cls.user)
        cls.working_plan = WorkingPlan()
        cls.working_plan.save()
        cls.provider.working_plan = cls.working_plan
        cls.provider.save()
        cls.provider.services.add(cls.service)
        cls.provider.save()

    def test_client_form_last_name(self):
        self.assertEqual(self.form.fields['last_name'].help_text, '')

    def test_can_post_clientform(self):
        # Create valid data, assert form is valid
        data = {
            'last_name': 'mar',
            'first_name': 'ilie',
            'date_time': '2017-12-20 15:16',
            'service': str(self.service.pk),
            'provider': str(self.provider.pk)
        }
        form = ClientForm(data, user=self.user)
        form.save()
        self.assertTrue(form.is_valid())

        # Create invalid data, assert form is invalid
        data = {
            'last_name': 'mar',
            'first_name': 'ilie',
            'date_time': '2017-12-20 15:',
            'service': str(self.service.pk),
            'provider': str(self.provider.pk)
        }
        form = ClientForm(data, user=self.user)
        with self.assertRaisesMessage(
                ValueError, "The Client could not be created because the data didn't validate."):
            form.save()
