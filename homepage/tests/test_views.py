from django.test import TestCase
from django.contrib.auth.models import User
from django.shortcuts import reverse
from django.utils import timezone

from homepage.forms import ClientForm
from homepage.models import Client, Provider, Service, WorkingPlan


class AddClientTest(TestCase):

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('homepage:homepage'))
        # Manually check redirect (Can't use assertRedirect,
        # because the redirect URL is unpredictable)
        self.assertEqual(resp.status_code, 302)
        self.assertTrue(resp.url.startswith('/accounts/login/'))

    # Client view begin
    def test_HTTP404_for_invalid_client_if_logged_in(self):
        test_pk = 9999999999999  # unlikely UID to match the client
        self.client.login(username='testuser2', password='12345')
        resp = self.client.get(
            reverse('clients:client-update', kwargs={'pk': test_pk, }))
        self.assertEqual(resp.status_code, 404)

    def test_uses_correct_template(self):
        # Create a user
        test_user1 = User.objects.create_user(username='testuser1',
                                              password='12345')
        test_user1.save()

        test_user2 = User.objects.create_user(
            username='testuser2', password='12345')
        test_user2.save()

        # Create service
        test_service = Service.objects.create(name='interne', user=test_user1)

        # Create a working plan
        working_plan = WorkingPlan()
        working_plan.save()

        # Create a provider
        test_provider = Provider(
            last_name='frag', first_name='fento',
            user=test_user1, working_plan=working_plan)
        test_provider.save()
        test_provider.services.add(test_service)
        test_provider.save()

        # Create a client
        date = timezone.now()
        test_client = Client(last_name='ben', first_name='hur',
                             date_time=date, service=test_service,
                             provider=test_provider)
        test_client.save()

        self.client.login(username='testuser2', password='12345')
        resp = self.client.get(
            reverse('clients:client-update',
                    kwargs={'pk': test_client.pk, }))

        self.assertEqual(resp.status_code, 200)

        # Check we used correct template
        self.assertTemplateUsed(resp, 'clients/client_form.html')
