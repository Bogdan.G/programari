$(document).ready(function () {
    // Global variables begin
    let site_address = window.location.origin;

    let businessHoursStartArr = business_hours_start.split(',');
    let businessHoursEndArr = business_hours_end.split(',');

    let breakDayArr = break_day.split(',');
    let breakBeginArr = break_begin.split(',');
    let breakEndArr = break_end.split(',');
    let breaksDayForDropdownArr = breaksDayForDropdown.split(',');
    breakDayArr.pop();
    breakBeginArr.pop();
    breakEndArr.pop();
    breaksDayForDropdownArr.pop();

    // Variables needed for setting the buttons
    // for time select in ClientForm
    let serviceDuration = 0;
    let hoursStartArr = [];
    let hoursEndArr = [];
    let brDayArr = [];
    let brBeginArr = [];
    let brEndArr = [];

    function strToMiliseconds(val) {
        valArr = val.split(":");
        hours = parseInt(valArr[0], 10);
        minutes = parseInt(valArr[1], 10);
        return ((hours * 3600) + (minutes * 60)) * 1000;
    }

    // Global variables end

    // FullCalendar setup begin
    $('#calendar').fullCalendar({
        // put your options and callbacks here
        defaultView: 'agendaWeek',
        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        },
        forceEventDuration: true,
        firstDay: 1,
        businessHours: [
            {
                dow: [1], // Monday
                start: businessHoursStartArr[0],
                end: businessHoursEndArr[0]
            },
            {
                dow: [2], // Tuesday
                start: businessHoursStartArr[1],
                end: businessHoursEndArr[1]
            },
            {
                dow: [3], // Wednesday
                start: businessHoursStartArr[2],
                end: businessHoursEndArr[2]
            },
            {
                dow: [4], // Thursday
                start: businessHoursStartArr[3],
                end: businessHoursEndArr[3]
            },
            {
                dow: [5], // Friday
                start: businessHoursStartArr[4],
                end: businessHoursEndArr[4]
            },
            {
                dow: [6], // Saturday
                start: businessHoursStartArr[5],
                end: businessHoursEndArr[5]
            },
            {
                dow: [0], // Sunday
                start: businessHoursStartArr[6],
                end: businessHoursEndArr[6],

            },

        ],

        // when event clicked go to client's detail page
        eventClick: function (calEvent, jsEvent, view) {

            if (calEvent.title != "Pauza") {
                urlPost = site_address + '/clienti/' + calEvent.clientId.toString() + "/";
                urlPost = urlPost.replace(/\s/, "");
                window.open(urlPost, "_self");
            }
        },

        // on mouseover change style
        eventMouseover: function (calEvent, jsEvent, view) {
            $(this).css("cursor", "pointer");
        }

    });
    // FullCalendar setup end

    // FullCalendar populate begin
    clientsPksArr = clientsPks.split(',');
    clientsNamesArr = clientsNames.split(',');
    clientsDatesArr = clientsDates.split(',');
    clientServicesArr = clientServices.split(',');
    clientServicesDurationArr = clientServicesDuration.split(',');
    clientServicesArr.pop();
    clientServicesDurationArr.pop();
    clientsPksArr.pop();
    clientsNamesArr.pop();
    clientsDatesArr.pop();

    arrSource = {
        events: [{}],

    };
    for (let i = 0; i < breaksDayForDropdownArr.length; i++) {
        arrSource.events.push({
            title: 'Pauza', start: breakBeginArr[i], end: breakEndArr[i],
            dow: [breaksDayForDropdownArr[i]]
        });
    }

    for (let i = 0; i < clientsNamesArr.length; i++) {
        let e = moment(clientsDatesArr[i]);
        e.add(moment.duration(clientServicesDurationArr[i]));
        console.log(e.format("YYYY-MM-DD HH:mm:ss"));
        arrSource.events.push({
            title: (clientsNamesArr[i] + ': ' + clientServicesArr[i]),
            start: clientsDatesArr[i],
            end: (e.format("YYYY-MM-DD HH:mm:ss")),
            clientId: clientsPksArr[i]
        });
    }

    $("#calendar").fullCalendar("addEventSource", arrSource);
    // FullCalendar populate end

    //  Get the csrf token's value for ajax calls
    let csrfToken = $("input[name='csrfmiddlewaretoken']").val();

    //  Set up for the ajax calls
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrfToken);
            }
        }
    });
    // End set up ajax

    // ClientForm change service, update providers
    $('#id_service').change(function () {

        //  Ajax call for client form(services and providers fields)
        let service_pk = $('#id_service').val();

        $.ajax({
            url: site_address,
            type: 'POST',
            data: {
                request: "service",
                service_pk: service_pk,
            },
            dataType: 'json',
            success: function (data) {
                let options = '';
                let providersArr = [];
                let providersPkArr = [];
                providersArr = data.providers.split(",");
                providersArr.pop();
                console.log(providersArr.length);
                providersPkArr = data.providers_pk.split(",");
                providersPkArr.pop();
                options = '<option value="" selected="">---------</option>';
                for (let i = 0; i < providersArr.length; i++) {
                    options += '<option value="' + providersPkArr[i] + '">' + providersArr[i] + '</option>';
                }
                $('#id_provider').html(options);

            }
        });
        // ajax end
    });
    // change service end

    // ClientForm change provider, update times
    $('#id_provider').change(function () {

        //  Ajax call for client form(services and providers fields)
        let provider_pk = $('#id_provider').val();
        let service_pk = $('#id_service').val();

        $.ajax({
            url: site_address,
            type: 'POST',
            data: {
                request: "provider",
                provider_pk: provider_pk,
                service_pk: service_pk,
            },
            dataType: 'json',
            success: function (data) {
                console.log(data.service_duration + '\n',
                    data.business_hours_start + '\n',
                    data.business_hours_end + '\n',
                    data.break_day + '\n',
                    data.break_begin + '\n',
                    data.break_end + '\n');

                let durationArr = data.service_duration.split(":");
                serviceDuration = parseInt((durationArr[0] * 60), 10) + parseInt(durationArr[1], 10);
                hoursStartArr = data.business_hours_start.split(",");
                console.log(hoursStartArr);
                hoursEndArr = data.business_hours_end.split(",");
                brDayArr = data.break_day.split(",");
                brBeginArr = data.break_begin.split(",");
                brEndArr = data.break_end.split(",");
                brDayArr.pop();
                brBeginArr.pop();
                brEndArr.pop();
            }
        });
        // ajax end
    });
    // change provider end

    // On selecting provider, show appointments and set
    // session variables for that provider 
    $('.dropdown-provider').click(function () {

        let selectedProvider = $(this).text();
        let providerPk = ($(this).val());

        // update dropdown text with the one clicked
        $('#dropdown-select-provider-button').text(selectedProvider);
        console.log("wnlnewnnnsdnd");

        $.ajax({
            url: site_address,
            type: 'POST',
            data: {
                request: "clients",
                provider_pk: providerPk,
            },
            dataType: 'json',
            success: function (data) {
                // clients pks, names and appointment info from the backend
                clientsPksArr = data.clientsPks.split(',');
                clientsNamesArr = data.clientsNames.split(',');
                clientsDatesArr = data.clientsDates.split(',');
                breaksDayForDropdownArr = data.day_of_break.split(',');
                breakDayArr = data.break_day.split(',');
                breakBeginArr = data.break_begin.split(',');
                breakEndArr = data.break_end.split(',');
                clientServicesArr = data.clientServices.split(',');
                clientServicesDurationArr = data.clientServicesDuration.split(',');
                clientServicesArr.pop();
                clientServicesDurationArr.pop();
                breaksDayForDropdownArr.pop();
                breakDayArr.pop();
                breakBeginArr.pop();
                breakEndArr.pop();
                clientsPksArr.pop();
                clientsNamesArr.pop();
                clientsDatesArr.pop();

                businessHoursStartArr = data.business_hours_start.split(',');
                businessHoursEndArr = data.business_hours_end.split(',');

                $('#calendar').fullCalendar('option', {
                    businessHours: [ // specify an array instead
                        {
                            dow: [1], // Monday
                            start: businessHoursStartArr[0],
                            end: businessHoursEndArr[0]
                        },
                        {
                            dow: [2], // Tuesday
                            start: businessHoursStartArr[1],
                            end: businessHoursEndArr[1]
                        },
                        {
                            dow: [3], // Wednesday
                            start: businessHoursStartArr[2],
                            end: businessHoursEndArr[2]
                        },
                        {
                            dow: [4], // Thursday
                            start: businessHoursStartArr[3],
                            end: businessHoursEndArr[3]
                        },
                        {
                            dow: [5], // Friday
                            start: businessHoursStartArr[4],
                            end: businessHoursEndArr[4]
                        },
                        {
                            dow: [6], // Saturday
                            start: businessHoursStartArr[5],
                            end: businessHoursEndArr[5]
                        },
                        {
                            dow: [0], // Sunday
                            start: businessHoursStartArr[6],
                            end: businessHoursEndArr[6],

                        },
                    ]
                });

                // clear events before adding new ones,
                // we want only the ones for this particular provider
                arrSource.events.splice(0);
                $("#calendar").fullCalendar('removeEvents');
                console.log(breakBeginArr);

                for (let i = 0; i < breaksDayForDropdownArr.length; i++) {
                    arrSource.events.push({
                        title: 'Pauza', start: breakBeginArr[i], end: breakEndArr[i],
                        dow: [breaksDayForDropdownArr[i]]
                    });
                }

                for (let i = 0; i < clientsNamesArr.length; i++) {
                    let e = moment(clientsDatesArr[i]);
                    e.add(moment.duration(clientServicesDurationArr[i]));
                    console.log(e);
                    arrSource.events.push({
                        title: (clientsNamesArr[i] + ': ' + clientServicesArr[i]),
                        start: clientsDatesArr[i],
                        end: (e.format("YYYY-MM-DD HH:mm:ss")),
                        clientId: clientsPksArr[i]
                    });
                }
                console.log(arrSource);

                $("#calendar").fullCalendar("addEventSource", arrSource);
            }

        });
        // dropdown ajax end

    });
    // dropdown click end

    $('#datetimepicker').datetimepicker({
        format: 'MM/dd/YYYY',
        inline: true,
        sideBySide: true,
        locale: 'ro',
        minDate: moment(),
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar-alt',
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right',
            today: 'fas fa-bullseye',
            clear: 'far fa-trash-alt',
            close: 'fas fa-times'
        },
    });

    $('#to-form').click(function () {
        $('#client-form-1').css('display', 'block');
        $('#footer-1').css('display', 'block');
        $('#client-form-2').css('display', 'none');
        $('#footer-2').css('display', 'none');
    });

    $('#to-datetime').click(function () {
        $('#client-form-1').css('display', 'none');
        $('#footer-1').css('display', 'none');
        $('#client-form-2').css('display', 'block');
        $('#footer-2').css('display', 'block');
    });

    $('#datetimepicker').on("dp.change", function (e) {

        let provider_pk = $('#id_provider').val();
        let service_pk = $('#id_service').val();
        let date_selected = e.date.format('YYYY-MM-DD');
        console.log(date_selected);

        // get the times to be displayed by the buttons
        $.ajax({
            url: site_address,
            type: 'POST',
            data: {
                request: "times",
                provider_pk: provider_pk,
                service_pk: service_pk,
                date_selected: date_selected,
            },
            dataType: 'json',
            success: function (data) {
                let buttonValue = "";
                let buttonHtml = "";
                times = data.times;
                console.log(times);
                for (let i = 0; i < times.length; i++) {
                    buttonValue = times[i];
                    buttonHtml += `
                    <div class="col mt-3">
                        <button type="button" class="btn btn-outline-dark times">${buttonValue}</button>
                    </div>
                    `;
                }
                $('#button-placeholder').html(buttonHtml);
                $('.times').click(function (e) {
                    console.log(e.target.textContent);
                    $('#id_date_time').val(date_selected + ' ' + e.target.textContent);
                });
            }
        });
        // ajax end

    });
    // datetimepicker view and logic end

    // Account logic start
    $("#sign-up-button").click(function () {
        console.log("aa");
    });
    $("#change-password").click(function () {
        window.location.href = $(this).val();
    });
    $("#home-from-password-change").click(function () {
        window.location.href = $(this).val();
    });
    $("#sign-up-button").click(function () {
        window.location.href = $(this).val();
    });
    $("#signup-back").click(function () {
        window.location.href = $(this).val();
    });
    // Account logic end

    // Forms validation start
    jQuery.validator.addMethod("usernameRegex", function (value, element) {
        return this.optional(element) || /[a-zA-Z\s-]+$/.test(value);
    }, "Introduceti valori valide");

    $("#add-appointment-form").validate({
        rules: {
            last_name: {
                required: true,
                usernameRegex: true
            },
            first_name: {
                required: true,
                usernameRegex: true
            },
        },
        messages: {
            last_name: {
                required: "Acest camp trebuie completat"
            },
            first_name: {
                required: "Acest camp trebuie completat"
            },
        }
    });
    // Forms validation end

    // timeDurationPicker start
    $('.timepicker').pickatime({
        format: 'HH:i',
        interval: 15,
        min: '07:30',
        max: '19:30',
    });

    // Provider update back button
    $('#provider-back').click(function (e) {
        e.preventDefault();
        window.location.href = $(this).val();
    });

    // timeDurationPicker end

    // let begin = '';
    // let end = '';

    // $('#id_day').change(function () {

    //     let day_selected = $(this).val();
    //     let provider_pk_arr = window.location.pathname.split("/");
    //     let provider_pk = provider_pk_arr[2];

    //     // get the times to be displayed by the buttons
    //     $.ajax({
    //         url: site_address,
    //         type: 'POST',
    //         data: {
    //             request: "day_for_breaks",
    //             provider_pk: provider_pk,
    //             day_selected: day_selected,
    //         },
    //         dataType: 'json',
    //         success: function (data) {
    //             console.log(data.begin);
    //             console.log(data.end);
    //             begin = data.begin.slice(0, -3);
    //             end = data.end;
    //             console.log(begin);
    //         }
    //     });
    //     // ajax end
    // });
    // // id_day logic end
    
    $('#id_time_begin').pickatime({
        format: 'HH:i',
        interval: 15,
        min: '07:30',
        max: '19:30',
        container: '#form-break'
    });
    
    $('#id_time_end').pickatime({
        format: 'HH:i',
        interval: 15,
        min: '07:30',
        max: '19:30',
        container: '#form-break'
    });

    $('#id_duration').pickatime({
        format: 'HH:i',
        min: '00:00',
        max: '02:00',
        interval: 5,
    });



});

// TODO: add duration and start/end inputs