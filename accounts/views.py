from django.shortcuts import render, redirect
from django.contrib.auth.views import (LoginView, PasswordChangeView,
                                       PasswordChangeDoneView)
from django.contrib.auth import login, authenticate                                
from django.views.generic import UpdateView
from django.contrib.auth.models import User
from django.urls import reverse_lazy

from homepage.forms import (UserLoginForm, UserAccountForm,
                            CustomPasswordChangeForm, CustomUserCreationForm)


class CustomLoginView(LoginView):

    form_client = UserLoginForm
    extra_context = {'form_client': form_client}


class UserUpdate(UpdateView):
    model = User
    form_class = UserAccountForm
    template_name = 'accounts/user_form.html'


class CustomPasswordChangeView(PasswordChangeView):
    form_class = CustomPasswordChangeForm
    template_name = 'accounts/password_change_form.html'
    success_url = reverse_lazy('accounts:password_change_done')


class CustomPasswordChangeDoneView(PasswordChangeDoneView):
    template_name = 'accounts/password_change_done.html'


def signup(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('homepage:homepage')
    else:
        form = CustomUserCreationForm()
    return render(request, 'accounts/sign_up.html', {'form': form})