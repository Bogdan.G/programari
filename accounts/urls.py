from django.urls import path
from django.contrib.auth import views as auth_views
from django.utils.translation import gettext_lazy as _

from . import views


urlpatterns = [
    path(_('login/'), views.CustomLoginView.as_view(),
         name='custom-login'),
    # after logout, we need to go back to the login page
    path(_('logout/'), auth_views.logout_then_login,
         name='logout'),
    path(_('<int:pk>/update-account/'),
         views.UserUpdate.as_view(),
         name='user-update'),
    path(_('<int:pk>/password_change/'),
         views.CustomPasswordChangeView.as_view(),
         name='password_change',),
    path(_('password_change_done/'),
         views.CustomPasswordChangeDoneView.as_view(),
         name='password_change_done'),
    path(_('sign-up/'), views.signup, name='signup'),
]
