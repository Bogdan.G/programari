from django.test import TestCase
from django.contrib.auth.models import User

from homepage.forms import CustomUserCreationForm


class AccountsViewTest(TestCase):

    def setUp(self):
        User.objects.create_user(username='bill1', password='1234')
        User.objects.create_user(username='bill2', password='1234')

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get('/')
        self.assertRedirects(resp, '/accounts/login/?next=/')

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='bill1', password='1234')
        resp = self.client.get('/')

        # Check the user is logged in
        self.assertEqual(str(resp.context['user']), 'bill1')
        # Check that we got a response "success"
        self.assertEqual(resp.status_code, 200)

        # Check we used correct template
        self.assertTemplateUsed(resp, 'homepage/calendar.html')

    def test_user_not_signed_up_not_let_in(self):
        self.client.login(username='bil', password='1234')
        resp = self.client.get('/')
        # We do not arrive
        self.assertRedirects(resp, '/accounts/login/?next=/')


class SignUpViewTesti(TestCase):

    def test_user_tries_to_sign_up(self):
        # Arrive at url
        resp = self.client.get('/accounts/sign-up/')
        # Correct template used for view
        self.assertTemplateUsed(resp, 'accounts/sign_up.html')
        # Data to populate form
        form_data1 = {'username': 'Kholn',
                      'email': 'kln@ssv.com',
                      'password1': 'eghjkllgg',
                      'password2': 'eghjkllgg', }
        # Create and save the form
        form = CustomUserCreationForm(data=form_data1)
        self.assertTrue(form.is_valid())

        form.save()

        # Test that the wrong user doesn't exist
        with self.assertRaisesMessage(User.DoesNotExist,
                                      'User matching query does not exist.'):
            User.objects.get(username='Khol')

        # Test that CustomUserCreationForm created
        # the expected user
        self.assertEqual(str(User.objects.get(username='Kholn')), 'Kholn')

        # Create a bad form and check that is invalid
        form_data2 = {'username': 'Kholn',
                      'email': 'kln@ssv',
                      'password1': 'eghj',
                      'password2': 'eghjkllgg', }
        # Create and save the form
        form = CustomUserCreationForm(data=form_data2)
        self.assertFalse(form.is_valid())

        # Assert valid form redirects to homepage
        form_data3 = {'username': 'Ven',
                      'email': 'von@ssv.com',
                      'password1': 'eghjklmpn',
                      'password2': 'eghjklmpn', }
        res = self.client.post('/accounts/sign-up/', data=form_data3)
        self.assertRedirects(res, '/')
