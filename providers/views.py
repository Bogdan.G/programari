from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import UpdateView, DeleteView
from django.utils.translation import gettext_lazy as _

from homepage.models import Provider, Service, Break, WorkingPlan
from homepage.forms import ProviderForm, BreakForm, WorkingPlanForm


def providers_view(request):

    user = request.user

    # if this is a POST request we need to provess the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form_provider = ProviderForm(request.POST, user=user)

        # check whether it's valid:
        if form_provider.is_valid():
            # process the data in form.cleaned_data as required
            # form.save()

            last_name = form_provider.cleaned_data['last_name']
            first_name = form_provider.cleaned_data['first_name']
            services = form_provider.cleaned_data['services']

            provider = Provider(
                last_name=last_name,
                first_name=first_name,
                user=user,
            )

            working_plan = WorkingPlan()
            working_plan.save()
            provider.working_plan = working_plan

            provider.save()

            for service in Service.objects.filter(pk__in=services):
                provider.services.add(service)

            provider.save()

            return HttpResponseRedirect('.')

    # if a GET (or any other method) we'll create a blank form
    else:
        form_provider = ProviderForm(user=user)
        services = user.service_set.all()
        providers = user.provider_set.all()

        return render(request, 'providers/providers.html',
                      {'form_provider': form_provider,
                       'providers': providers,
                       'services': services})

    return render(request, 'providers/providers.html',
                  {'form_provider': form_provider})


def provider_update(request, *args, **kwargs):

    user = request.user

    if request.method == 'POST':

        provider = Provider.objects.get(pk=kwargs['pk'])
        breaks = provider.break_set.all()

        days = [_('Monday'), _('Tuesday'), _('Wednesday'), _('Thursday'),
                _('Friday'), _('Saturday'), _('Sunday')]

        if "day" not in request.POST:
            form_provider = ProviderForm(request.POST, user=user)
            form_working_plan = WorkingPlanForm(request.POST)

            if form_provider.is_valid() and form_working_plan.is_valid():

                working_plan = WorkingPlan(
                    monday_start=form_working_plan.cleaned_data['monday_start'],
                    monday_end=form_working_plan.cleaned_data['monday_end'],
                    tuesday_start=form_working_plan.cleaned_data['tuesday_start'],
                    tuesday_end=form_working_plan.cleaned_data['tuesday_end'],
                    wednesday_start=form_working_plan.cleaned_data['wednesday_start'],
                    wednesday_end=form_working_plan.cleaned_data['wednesday_end'],
                    thursday_start=form_working_plan.cleaned_data['thursday_start'],
                    thursday_end=form_working_plan.cleaned_data['thursday_end'],
                    friday_start=form_working_plan.cleaned_data['friday_start'],
                    friday_end=form_working_plan.cleaned_data['friday_end'],
                    saturday_start=form_working_plan.cleaned_data['saturday_start'],
                    saturday_end=form_working_plan.cleaned_data['saturday_end'],
                    sunday_start=form_working_plan.cleaned_data['sunday_start'],
                    sunday_end=form_working_plan.cleaned_data['sunday_end'],
                )

                working_plan.save()

                last_name = form_provider.cleaned_data['last_name']
                first_name = form_provider.cleaned_data['first_name']
                services = form_provider.cleaned_data['services']
                provider.last_name = last_name
                provider.first_name = first_name
                provider.user = user

                provider.working_plan = working_plan
                provider.save()
                for service in provider.services.all():
                    provider.services.remove(service)
                for service in Service.objects.filter(pk__in=services):
                    provider.services.add(service)
                provider.save()
                return HttpResponseRedirect('.')

            else:
                form_break = BreakForm()
                form_working_plan = WorkingPlanForm(request.POST)
                return render(request, 'providers/provider_form.html',
                              {'form_provider': form_provider,
                               'form_working_plan': form_working_plan,
                               'form_break': form_break,
                               'breaks': breaks,
                               'days': days, })

        if "day" in request.POST:

            form_break = BreakForm(request.POST)

            if form_break.is_valid():
                provider = Provider.objects.get(pk=kwargs['pk'])

                br = Break(day=form_break.cleaned_data["day"],
                           time_begin=form_break.cleaned_data["time_begin"],
                           time_end=form_break.cleaned_data["time_end"],
                           provider=provider)
                br.save()

                return HttpResponseRedirect('.')

            else:
                form_provider = ProviderForm(
                    user=user,
                    initial={'last_name': provider.last_name,
                             'first_name': provider.first_name,
                             'services': provider.services.all(),
                             })
                form_working_plan = WorkingPlanForm(
                    initial={
                        'monday_start': provider.working_plan.monday_start,
                        'monday_end': provider.working_plan.monday_end,
                        'tuesday_start': provider.working_plan.tuesday_start,
                        'tuesday_end': provider.working_plan.tuesday_end,
                        'wednesday_start': provider.working_plan.wednesday_start,
                        'wednesday_end': provider.working_plan.wednesday_end,
                        'thursday_start': provider.working_plan.thursday_start,
                        'thursday_end': provider.working_plan.thursday_end,
                        'friday_start': provider.working_plan.friday_start,
                        'friday_end': provider.working_plan.friday_end,
                        'saturday_start': provider.working_plan.saturday_start,
                        'saturday_end': provider.working_plan.saturday_end,
                        'sunday_start': provider.working_plan.sunday_start,
                        'sunday_end': provider.working_plan.sunday_end,
                    }
                )

        return render(request, 'providers/provider_form.html',
                      {'form_provider': form_provider,
                       'form_working_plan': form_working_plan,
                       'form_break': form_break,
                       'breaks': breaks,
                       'days': days, })

    else:
        provider = Provider.objects.get(pk=kwargs['pk'])
        breaks = provider.break_set.all()

        form_working_plan = WorkingPlanForm(
            initial={
                'monday_start': provider.working_plan.monday_start,
                'monday_end': provider.working_plan.monday_end,
                'tuesday_start': provider.working_plan.tuesday_start,
                'tuesday_end': provider.working_plan.tuesday_end,
                'wednesday_start': provider.working_plan.wednesday_start,
                'wednesday_end': provider.working_plan.wednesday_end,
                'thursday_start': provider.working_plan.thursday_start,
                'thursday_end': provider.working_plan.thursday_end,
                'friday_start': provider.working_plan.friday_start,
                'friday_end': provider.working_plan.friday_end,
                'saturday_start': provider.working_plan.saturday_start,
                'saturday_end': provider.working_plan.saturday_end,
                'sunday_start': provider.working_plan.sunday_start,
                'sunday_end': provider.working_plan.sunday_end,
            }
        )

        days = [_('Monday'), _('Tuesday'), _('Wednesday'), _('Thursday'),
                _('Friday'), _('Saturday'), _('Sunday')]

        form_provider = ProviderForm(
            user=user,
            initial={'last_name': provider.last_name,
                     'first_name': provider.first_name,
                     'services': provider.services.all(),
                     })
        form_break = BreakForm()

        return render(request, 'providers/provider_form.html',
                      {'form_provider': form_provider,
                       'form_working_plan': form_working_plan,
                       'form_break': form_break,
                       'breaks': breaks,
                       'days': days, })


class ProviderDelete(DeleteView):
    model = Provider
    success_url = reverse_lazy('providers:providers')


class BreakDelete(DeleteView):
    model = Break

    def get_object(self, queryset=None):
        obj = super(BreakDelete, self).get_object()
        provider = Provider.objects.get(id=obj.provider.id)
        self.provider = provider
        return obj

    def get_success_url(self):
        return reverse_lazy('providers:provider-update',
                            kwargs={'pk': self.provider.pk})

# TODO: logic if inputs blank

# TODO: make all models hard to delete
