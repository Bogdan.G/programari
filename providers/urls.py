from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

urlpatterns = [
    path('<int:pk>/', views.provider_update,
         name='provider-update'),
    path(_('<int:pk>/delete/'), views.ProviderDelete.as_view(),
         name='provider-delete'),
    path(_('<int:pk>/break/delete'), views.BreakDelete.as_view(),
         name='break-delete'),
    path('', views.providers_view, name='providers'),
]
