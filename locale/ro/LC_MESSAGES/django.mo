��    j      l  �   �      	     	     '	     8	     R	     k	     r	     �	  	   �	     �	     �	     �	     �	  :   �	  8   �	     5
     ;
     B
     K
     [
     b
     j
  (   p
     �
     �
     �
     �
               -     ;     ?     H     L  v   T  &   �  
   �     �          
  .     	   >     H  
   O     Z     a     h     m  %   z     �     �  "   �  #   �     �            	   (     2     ;     D     K     [     c     l     �     �     �  *   �     �     �     �     �  	   �       /        C  :   V  	   �     �     �     �     �     �     �     �     �  
   �  	   �     �     �                    &     ,  	   5  
   ?     J     Z     b  	   k     u  
   ~     �     �     �  �  �     9     S     g     �     �     �     �     �  
   �     �     �     �  :   �  H   9     �     �     �     �     �     �     �  :   �     �       !   :  "   \          �     �     �     �  	   �     �  �   �  3   u     �     �     �     �  1   �     �     �                           (   ,     U     b     j     �  #   �     �     �  	   �     �  
   �     �     �               %  	   @  
   J  	   U  "   _      �     �     �     �     �     �  %   �     �  B        W     `     g     m     t  	   }     �     �     �     �     �     �     �  	   �     �     �     �       	     
        !     6     ?  	   H     R     ^     l  
   {     �         d          !   U       X      O   *                   g                         f   C             T   R               K          Q   J   )       H       3   W       B   :       $                     9   (      5               i   ;       A   6   F       b   =   M   P   >             Z   2   ^   &   "   <      %   S   _   ?   
       0   ,             j       	      c   `       .           '   @   +          N   a                  -   V              8   h   e   D      ]   1   #      Y      G      L   I   E      [       \          4          /         7    <int:pk>/break/delete <int:pk>/delete/ <int:pk>/password_change/ <int:pk>/update-account/ Accept Account settings Add appointment Add break Appointment Appointments Back Begin Beginning time of the break must be less than ending time. Beginning time of workday must be less than ending time. Break Breaks Calendar Change password Client Clients Close Complete the following fields to sign up Confirm deletion of break Confirm deletion of client Confirm deletion of provider Confirm deletion of service Confirm new password Confirm password Date and time Day Duration End English Enter the old password, for security reasons, then the new password two times, to check that it was entered correctly. Fill in the following fields to log in First name Found Friday Home Introduce the same password, for verification. Last name Log in Logged out Logout Monday Name New password No result found for the item searched Old password Password Please correct the following error Please correct the following errors Please enter a search term. Price Provider Providers Romanian Saturday Search Select provider Service Services Show appointments for: Sign up Sign-up Sunday The password has been changed successfully The passwords are different. Thursday Tuesday Username Wednesday Working plan You have been logged out successfully. You can  You searched for:  Your username and password do not match. Please try again. accounts/ break breaks client clients clients/ date and time day duration first name last name login again login/ logout/ name password_change_done/ price provider providers providers/ search-results/ service services services/ sign-up/ time begin time end user working plan Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-18 13:07+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 <int:pk>/pauză/ștergere <int:pk>/ștergere/ <int:pk>/schimbare_parolă/ <int:pk>/actualizare-cont/ Acceptă Setări cont Adaugă programare Adaugă pauză Programare Programări Înapoi Început Începutul pauzei trebuie să fie înaintea sfârșitului. Începutul programului de lucru trebuie să fie înaintea sfârșitului. Pauză Pauze Calendar Schimbă parola Client Clienți Închide Completați următoarele câmpuri pentru a vă înregistra Confirmați ștergerea pauzei Confirmă ștergerea clientului Confirmă ștergerea furnizorului Confirmați ștergerea serviciului Confirmați noua parolă Confirmați parola Data și timpul Ziua Durată Sfârșit Engleză Introduceți vechea parolă, din motive de securitate, după aceea, introduceți noua parola de două ori, pentru a verifica dacă celedouă sunt la fel. Completați următoarele câmpuri pentru a vă loga Prenume Găsit Vineri Acasă Introduceți aceeași parolă, pentru verificare. Nume Logare Delogat Delogare Luni Nume Parola nouă Nici un rezultat pentru obiectul căutat Parola veche Parolă Corectați următoarea eroare Corectați următoarele erori Introduceți un termen de căutare. Preț Furnizor Furnizori Română Sâmbătă Caută Selectează furnizor Serviciu Servicii Arată programări pentru: Inscriere Înscriere Duminică Parola a fost schimbată cu succes Cele două parole sunt diferite. Joi Marți Nume utilizator Miercuri Program de lucru Ați fost delogat cu succes. Puteți  Ați căutat pentru:  Numele de utilizator și parola nu corespund. Încercați din nou. conturi/ pauză pauze client clienți clienți/ data și timpul ziua durată Prenume Nume să vă logați din nou logare/ delogare/ nume schimbare_parolă_finalizat/ preț furnizor furnizori furnizori/ rezultate-căutări/ serviciu servicii servicii/ înscriere/ timp început timp sfârșit utilizator program de lucru 